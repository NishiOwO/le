#ifndef __LE_LANGUAGE_H__
#define __LE_LANGUAGE_H__
/*
 * Version (float), target, Date when this release was made (char*), Compilation date (char*), Compilation time (char*)
 */
#define LE_MSG_VERINFO_JA_JP "le - Line Editor %.2f-%s (%s, %s %sにコンパイル)\n"
#define LE_MSG_VERINFO_C "le - Line Editor %.2f-%s (%s, compiled %s %s)\n"
#define LE_MSG_VERINFO_SR_RS "le - Line Editor %.2f-%s (%s, compiled %s %s)\n"

#define LE_MSG_USAGE_JA_JP "使用法: %s [オプション] [ファイル名]\n"
#define LE_MSG_USAGE_C "Usage: %s [options] [file]\n"
#define LE_MSG_USAGE_SR_RS "Uputstvo: %s [opcije] [datoteka]\n"

#define LE_MSG_OPTION_JA_JP "オプション:\n"
#define LE_MSG_OPTION_C "Options:\n"
#define LE_MSG_OPTION_SR_RS "Opcije:\n"

#define LE_MSG_ONLY_FILENAME_JA_JP "  --               このあとにはファイル名だけ\n"
#define LE_MSG_ONLY_FILENAME_C "  --               Only file names after this\n"
#define LE_MSG_ONLY_FILENAME_SR_RS "  --              Samo ime datoteka posle ovoga\n"

#define LE_MSG_HELP_JA_JP "  --help      -h   この情報を表示する\n"
#define LE_MSG_HELP_C "  --help      -h   Display this information\n"
#define LE_MSG_HELP_SR_RS "  --help      -h  Uputstvo za koriščenje programa\n"

#define LE_MSG_VERSION_JA_JP "  --version   -V   leのバージョンを表示する\n"
#define LE_MSG_VERSION_C "  --version   -V   Display the version of the le\n"
#define LE_MSG_VERSION_SR_RS "  --version   -V  Prikaži verziju le programa\n"

#define LE_MSG_COPYRIGHT_JA_JP "Copyright (C) %d NishiOwO.\n"
#define LE_MSG_COPYRIGHT_C "Copyright (C) %d NishiOwO.\n"
#define LE_MSG_COPYRIGHT_SR_RS "Copyright (C) %d NishiOwO.\n"

#define LE_MSG_LICENSE_JA_JP "leは3条項BSDライセンス下に配布されています\n"
#define LE_MSG_LICENSE_C "le is licensed under the 3-clause BSD License\n"
#define LE_MSG_LICENSE_SR_RS "le je licensiran pod BSD licensom od 3 klauzule\n"

#define LE_MSG_LINES_JA_JP "%sは%d行です\n"
#define LE_MSG_LINES_C "%s, %d lines\n"
#define LE_MSG_LINES_SR_RS "%s, %d red\n"

#define LE_MSG_NOFILE_JA_JP "ファイルなしで立ち上げられました"
#define LE_MSG_NOFILE_C "No file is open"
#define LE_MSG_NOFILE_SR_RS "Nijedna datoteka nije otvorena"

#define LE_MSG_NEWFILE_JA_JP "は新しいファイルです"
#define LE_MSG_NEWFILE_C ", new file"
#define LE_MSG_NEWFILE_SR_RS ", nova datoteka"

#define LE_MSG_ERROR_JA_JP "エラー\n"
#define LE_MSG_ERROR_C "?Error\n"
#define LE_MSG_ERROR_SR_RS "?Greška\n"

#define LE_MSG_CHANGED_JA_JP "ファイル内容が変更されています。保存しますか？ [Y/N/C] "
#define LE_MSG_CHANGED_C "File is changed. Save? [Y/N/C] "
#define LE_MSG_CHANGED_SR_RS "Datoteka je bila izmenjena. Da li želite da sačuvate vaše promene? [Y/N/C] "

#define LE_MSG_YORNORC_JA_JP "YかNかCを打って下さい "
#define LE_MSG_YORNORC_C "Type Y or N or C "
#define LE_MSG_YORNORC_SR_RS "Ukucajte Y ili N ili C "

#define LE_MSG_ALRDYOPEN_JA_JP "ファイルが既に開かれています\n"
#define LE_MSG_ALRDYOPEN_C "File is already open\n"
#define LE_MSG_ALRDYOPEN_SR_RS "Datoteka je već otvorena\n"

#define LE_MSG_OVER1_JA_JP "行数は1か1以上である必要があります\n"
#define LE_MSG_OVER1_C "Line number must be equal to 1 or more\n"
#define LE_MSG_OVER1_SR_RS "Broj redosleda mora biti jednak 1 ili više\n"

#define LE_MSG_REGEX_SYNERR_JA_JP "構文エラー\n"
#define LE_MSG_REGEX_SYNERR_C "Syntax Error\n"
#define LE_MSG_REGEX_SYNERR_SR_RS "Greška u sintaksi redovnog izraza\n"

#define LE_MSG_REGEX_NOMATCHES_JA_JP "見つかりませんでした\n"
#define LE_MSG_REGEX_NOMATCHES_C "No matches\n"
#define LE_MSG_REGEX_NOMATCHES_SR_RS "Nema rezultata\n"

#define LE_MSG_ARGNEEDED_JA_JP "引数が必要です\n"
#define LE_MSG_ARGNEEDED_C "An argument is needed\n"
#define LE_MSG_ARGNEEDED_SR_RS "Potreban je argument\n"
#endif
