#ifndef __LE_LE_H__
#define __LE_LE_H__

#if defined(__NetBSD__)
#define LE_TARGET "netbsd"
#elif defined(__OpenBSD__)
#define LE_TARGET "openbsd"
#elif defined(__FreeBSD__)
#define LE_TARGET "freebsd"
#elif defined(__linux__)
#define LE_TARGET "linux"
#elif defined(__WIN64__)
#define LE_TARGET "win64"
#elif defined(__WIN32__)
#define LE_TARGET "win32"
#elif defined(__lyre__)
#define LE_TARGET "lyre"
#elif defined(__sun__)
#define LE_TARGET "sunos"
#elif defined(__HAIKU__)
#define LE_TARGET "haiku"
#elif defined (__unix__)
#define LE_TARGET "unix-unknown"
#else
#define LE_TARGET "unknown"
#endif

#include <language.h>

#define LE_SUCCESS 0
#define LE_ERROR 1
#define LE_EXIT 2
#define LE_NOSAVEEXIT 3
#define LE_SAVEEXIT 4
#define LE_SKIPNEXT 5

#define LE_NOINFO 0
#define LE_NEW 1

#define LE_VERSION 1.40
#define LE_DATE "2023 Apr 16th"
#define LE_COPYRIGHT 2023

struct le_mode {
	char insert;
	char replace;
	char edited;
};

struct le_status {
	char status;
	struct le_mode* mode;
};

struct le_status* le_process(char* data, struct le_mode* mode);
int le_open(char* path, int* lines);
void le_close();
void le_save();
char* le_get_message(char* token);
void le_set_quiet(char quiet);

#if defined(NO_STRDUP)
char* strdup(char* str);
#endif

#if defined(NO_GETLINE)
size_t getline(char**, size_t*, FILE*);
#endif

#endif
