# le - Line Editor
Simple Line Editor made by Nishi.

Gits:
 - [Ci622 CGit](http://ci622.kozow.com:5976/git/le/.git)
 - [EEE++ tEam Gitlab](http://git.e3t.cc/~nishiowo/le)
 - [Gitlab](http://gitlab.com/nishiowo/le)

## Usage
See `le(1)`.
```
LE(1)			    General Commands Manual			 LE(1)

NAME
     le - Line Editor

SYNOPSIS
     le [-Vhq] [--] [file]

DESCRIPTION
     le is a line-oriented text editor. It is used to create, display, modify
     text files. If invoked with argument, then a copy of file is read into
     the editor's buffer. Changes are made to this copy and not directly to
     file itself. Upon quitting le, quitting any changed not explicitly saved
     with sv, sq, or the prompt from q command are lost.

     A typical command might look like:

	  c 5

     which moves the line cursor to the line number 5.

     When an input command, such as i (insert), or r (replace) is given, le
     enters input mode. This is the primary means of adding text to a file. In
     this mode, no commands are available; instead, the standard input is
     written directry to the editor buffer. Lines consist of text up to and
     including newline character. Input mode is terminated by entering a
     single period (`.`) on a line.

     In general, le commands consist of a single character command (or the
     "long" command), followed by zero or more parameters; i.e., commnds have
     the structure:

	  command [parameter]


OPTIONS
     --		       Only file nmaes after this

     -h		       Display the help information

     -V		       Display the version of the le

     -q		       Launch le with quiet mode.

COMMAND
     r/replace [starting-line]	       Enter Replace mode.

				       It will set the "Current Line" register
				       to starting-line if the argument is
				       passed.

     i/insert [starting-line]	       Enter Insert mode.

				       It will set the "Current Line" register
				       to starting-line if the argument is
				       passed.

     s/show [base-line[+lines|-lines]]
				       Show line(s).

				       It will show the current line content
				       if no arguments are passed.

				       +lines means printing lines lines from
				       base-line.

				       -lines means printing lines lines from
				       base-line but backwards.

				       c in base-line or lines will be
				       replaced with the "Current Line"
				       register value.

				       l in base-line or lines will be
				       replaced with the latest line number.

     sa/showall			       Show all lines.

     pa/putafter [line]		       Puts the newline after line.

     d/delete [base-line[+lines|-lines]]
				       Delete line(s).

				       It will delete the current line content
				       if no arguments are passed.

				       +lines means deleting lines lines from
				       base-line.

				       -lines means deleting lines lines from
				       base-line but backwards.

				       c in base-line or lines will be
				       replaced with the "Current Line"
				       register value.

				       l in base-line or lines will be
				       replaced with the latest line number.

     c/current [+|-|][line]	       Get the "Current Line" register if no
				       arguments are passed.

				       Set the "Current Line" register to line
				       if no signs are set.

				       Increase/Decrease "Current Line"
				       register by line if sign is set.

     rs/regexsearch [pattern]	       Search things with RegEx.

     rss/regexsearchseek [pattern]     Search things with RegEx, and set the
				       "Current Line" register to the matched
				       line.

     sh/shell [command]		       Run shell command.

     o/open [filename]		       Open the file filename.

     q/quit			       Exit le.

				       Will ask you to select Yes, No, or
				       Cancel if the editor's buffer is
				       changed.

     sq/savequit		       Save the editor's buffer to the file,
				       and exit le.

     nsq/nosavequit		       Do not save the editor's buffer to the
				       file, and exit le.

     sv/save			       Save the editor's buffer to the file.

     v/version			       Display the version of the le

     cl/clear			       Clear the screen.

COPYRIGHT
     le is written by NishiOwO and contributors.

     Copyright (C) 2023 NishiOwO

     le is free software; you can redistribute it and/or modify it under the
     terms of the BSD 3-clause License.

AUTHORS
     NishiOwO nishi@e3t.cc
		       Developer

     re9177 g268268268@outlook.com
		       Serbian (Latin) Translator

     RaidTheWeb raidtheweb@disr.it
		       Lyre support

NetBSD 10.0_BETA	    2023 January 29th (JST)	      NetBSD 10.0_BETA
```

## How to build le
NOTE: `./configure` will try to use MUSL first.

```shellsession
$ ./configure --prefix=/usr
$ gmake all
```
