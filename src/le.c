#ifdef __linux__
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <locale.h>

#if defined(WIN32) || defined(__MINGW32__)
#define WINDOWS
#define NO_GETLINE
#endif

#ifndef WINDOWS
#include <regex.h>
#endif

#include <le.h>

#if defined(WIN32) || defined(__MINGW32__)
#include <winnls.h>
#warning "RegEx support is dropped on Windows build"
#endif

#define USR1 (void*)1

FILE* le_file;
char* le_path;
int le_current_line;
char** le_buffer;
int le_counter;
int le_lines;
char le_quiet;
char** le_insert_buffer;
int le_insert_begin;

struct le_mode* le_global_mode;

struct le_status* le_process(char* data, struct le_mode* mode){
	le_global_mode = mode;
	struct le_status* status = malloc(sizeof(*status));
	status->mode = mode;
	int count;
	for(count = 0; count < strlen(data) && data[count] != ' '; count++);
	char* command = malloc(count + 1);
	command[count] = 0;

	char* argument = malloc(strlen(data) - (count + 1) + 1);
	if(strlen(data) - (count + 1) != -1) argument[strlen(data) - (count + 1)] = 0;

	memcpy(command, data, count);
	if(strlen(data) - (count + 1) != -1){
		memcpy(argument, data + count + 1, strlen(data) - (count + 1) + 1);
	}else{
		free(argument);
		argument = malloc(1);
		argument[0] = 0;
	}
	memset(status, 0, sizeof(*status));

	status->status = LE_SUCCESS;
	if(mode->replace){
		if(strcmp(data, ".") == 0){
			mode->replace = 0;
		}else{
			mode->edited = 1;
			le_buffer = realloc(le_buffer, sizeof(*le_buffer) * (le_lines + 2));
			le_buffer[le_current_line + le_counter] = malloc(strlen(data) + 1);
			le_buffer[le_current_line + le_counter][strlen(data)] = 0;
			if(le_current_line + le_counter > le_lines) le_buffer[le_current_line + le_counter + 1] = NULL;
			strcpy(le_buffer[le_current_line + le_counter], data);
			le_counter++;
			if(!le_quiet) printf("R%6d+%6d: ", le_current_line + 1, le_counter);
		}
	}else if(mode->insert){
		if(strcmp(data, ".") == 0){
			int lines = le_lines + le_counter;
			char** insert_buffer = malloc(sizeof(*insert_buffer) * (1 + lines));
			int i;
			for(i = 0; i < lines; i++){
				if(i < le_insert_begin){
					insert_buffer[i] = strdup(le_buffer[i]);
					free(le_buffer[i]);
				}else if(i < (le_insert_begin + le_counter)){
					insert_buffer[i] = strdup(le_insert_buffer[i - le_insert_begin]);
					free(le_insert_buffer[i - le_insert_begin]);
				}else{
					insert_buffer[i] = strdup(le_buffer[i - le_counter]);
					free(le_buffer[i - le_counter]);
				}
			};
			free(le_buffer);
			le_buffer = insert_buffer;
			insert_buffer[lines] = NULL;
			le_lines = lines;
			free(le_insert_buffer);
			mode->insert = 0;
		}else{
			le_insert_buffer = realloc(le_insert_buffer, sizeof(*le_insert_buffer) * (le_counter + 2));
			le_insert_buffer[le_counter] = malloc(strlen(data) + 1);
			strcpy(le_insert_buffer[le_counter], data);
			le_insert_buffer[le_counter][strlen(data)] = 0;
			le_insert_buffer[le_counter + 1] = NULL;
			mode->edited = 1;
			le_counter++;
			if(!le_quiet) printf("I%6d+%6d: ", le_current_line + 1, le_counter);
		}
	}else{
		if(strcmp(command, "replace") == 0 || strcmp(command, "r") == 0){
			if(strlen(argument) != 0){
				if(atoi(argument) <= 0){
					fprintf(stderr, "%s", le_get_message("OVER1"));
					status->status = LE_ERROR;
					goto skip_replace;
				}else{
					le_current_line = atoi(argument) - 1;
				}
			}
			if(le_current_line + 1 > le_lines){
				le_buffer = realloc(le_buffer, sizeof(*le_buffer) * (2 + le_lines + le_current_line - le_lines));
				if(le_current_line - le_lines > 0){
					int i;
					for(i = le_lines; i < le_current_line + 1; i++){
						le_buffer[i] = malloc(1);
						le_buffer[i][0] = 0;
						le_buffer[i + 1] = NULL;
					}
					le_lines = le_current_line;
				}
			}
			mode->replace = 1;
			le_counter = 0;
			if(!le_quiet) printf("R%6d+%6d: ", le_current_line + 1, 0);
skip_replace:;
		}else if(strcmp(command, "insert") == 0 || strcmp(command, "i") == 0){
			if(strlen(argument) != 0){
				if(atoi(argument) <= 0){
					fprintf(stderr, "%s", le_get_message("OVER1"));
					status->status = LE_ERROR;
					goto skip_insert;
				}else{
					le_current_line = atoi(argument) - 1;
				}
			}
			if(le_current_line + 1 > le_lines){
				le_buffer = realloc(le_buffer, sizeof(*le_buffer) * (2 + le_lines + le_current_line - le_lines));
				if(le_current_line - le_lines > 0){
					int i;
					for(i = le_lines; i < le_current_line + 1; i++){
						le_buffer[i] = malloc(1);
						le_buffer[i][0] = 0;
						le_buffer[i + 1] = NULL;
					}
					le_lines = le_current_line;
				}
			}
			mode->insert = 1;
			le_insert_begin = le_current_line;
			le_counter = 0;
			le_insert_buffer = malloc(sizeof(*le_insert_buffer));
			le_insert_buffer[0] = NULL;
			if(!le_quiet) printf("I%6d+%6d: ", le_current_line + 1, 0);
skip_insert:;
		}else if(strcmp(command, "quit") == 0 || strcmp(command, "q") == 0){
			status->status = LE_EXIT;
		}else if(strcmp(command, "nosavequit") == 0 || strcmp(command, "nsq") == 0){
			status->status = LE_NOSAVEEXIT;
		}else if(strcmp(command, "savequit") == 0 || strcmp(command, "sq") == 0){
			status->status = LE_SAVEEXIT;
		}else if(strcmp(command, "show") == 0 || strcmp(command, "s") == 0){
			int line = 0;
			int baseline = 0;
			char range = 0;
			int range_index = 0;
			int i;
			for(i = 0; i < strlen(argument); i++){
				if(argument[i] == '+'){
					range = 1;
					range_index = i;
					break;
				}else if(argument[i] == '-'){
					range = -1;
					range_index = i;
					break;
				}
			}
			if(strlen(argument) == 0){
				baseline = le_current_line + 1;
				line = 1;
			}else if(range == 0){
				baseline = atoi(argument);
				if(strcmp(argument, "c") == 0){
					baseline = le_current_line + 1;
				}else if(strcmp(argument, "l") == 0){
					baseline = le_lines;
				}
				line = 1;
			}else{
				argument[range_index] = 0;
				baseline = atoi(argument);
				if(strcmp(argument, "c") == 0){
					baseline = le_current_line + 1;
				}else if(strcmp(argument, "l") == 0){
					baseline = le_lines;
				}
				line = atoi(argument + range_index + 1);
				if(range == -1){
					baseline -= line - 1;
					line--;
				}
			}
			line--;
			for(i = 0; i < baseline + line && le_buffer[i] != NULL; i++){
				size_t size = 0;
				char* line_str = le_buffer[i];
				if(i >= baseline - 1){
					if(!le_quiet) printf("%6d: %s\n", i + 1, line_str);
				}
			}
		}else if(strcmp(command, "current") == 0 || strcmp(command, "c") == 0){
			if(strlen(argument) != 0){
				if(argument[0] == '-' || argument[0] == '+'){
					if(le_current_line + atoi(argument) <= 0){
						fprintf(stderr, "%s", le_get_message("OVER1"));
						status->status = LE_ERROR;
					}else{
						le_current_line += atoi(argument);
					}
				}else{
					if(atoi(argument) <= 0){
						fprintf(stderr, "%s", le_get_message("OVER1"));
						status->status = LE_ERROR;
					}else{
						le_current_line = atoi(argument) - 1;
					}
				}
			}
			if(le_current_line + 1 > le_lines){
				le_buffer = realloc(le_buffer, sizeof(*le_buffer) * (2 + le_lines + le_current_line - le_lines));
				if(le_current_line - le_lines > 0){
					int i;
					for(i = le_lines; i < le_current_line + 1; i++){
						le_buffer[i] = malloc(1);
						le_buffer[i][0] = 0;
						le_buffer[i + 1] = NULL;
					}
					le_lines = le_current_line;
				}
			}
			if(!le_quiet && status->status != LE_ERROR) printf("%6d\n", le_current_line + 1);
		}else if(strcmp(command, "showall") == 0 || strcmp(command, "sa") == 0){
			if(!le_quiet){
				int i;
				for(i = 0; le_buffer[i] != NULL; i++) printf("%6d: %s\n", i + 1, le_buffer[i]);
			}
		}else if(strcmp(command, "clear") == 0 || strcmp(command, "cl") == 0){
			printf("\x1b[2J\x1b[0;0H");
			fflush(stdout);
		}else if(strcmp(command, "save") == 0 || strcmp(command, "sv") == 0){
			le_save();
		}else if(strcmp(command, "open") == 0 || strcmp(command, "o") == 0){
			if(le_path != NULL){
				if(!le_quiet) fprintf(stderr, "%s", le_get_message("ALRDYOPEN"));
				status->status = LE_ERROR;
			}else{
				int lines;
				if(le_open(argument, &lines) == LE_NOINFO){
					if(!le_quiet){
						printf(le_get_message("LINES"), le_path, lines);
					}
				}else{
					if(!le_quiet) printf("%s%s\n", le_path, le_get_message("NEWFILE"));
				}
			}
		}else if(strcmp(command, "version") == 0 || strcmp(command, "v") == 0){
			printf(le_get_message("VERINFO"), LE_VERSION, LE_TARGET, LE_DATE, __DATE__, __TIME__);
		}else if(strcmp(command, "shell") == 0 || strcmp(command, "sh") == 0){
#ifndef NOSHELL
			if(strlen(argument) == 0){
				system(getenv("SHELL"));
			}else{
				system(argument);
			}
#else
			printf("Disabled\n");
#endif
		}else if(strcmp(command, "delete") == 0 || strcmp(command, "d") == 0){
			int line = 0;
			int baseline = 0;
			char range = 0;
			int range_index = 0;
			int i;
			for(i = 0; i < strlen(argument); i++){
				if(argument[i] == '+'){
					range = 1;
					range_index = i;
					break;
				}else if(argument[i] == '-'){
					range = -1;
					range_index = i;
					break;
				}
			}
			if(strlen(argument) == 0){
				baseline = le_current_line + 1;
				line = 1;
			}else if(range == 0){
				baseline = atoi(argument);
				if(strcmp(argument, "c") == 0){
					baseline = le_current_line + 1;
				}else if(strcmp(argument, "l") == 0){
					baseline = le_lines;
				}
				line = 1;
			}else{
				argument[range_index] = 0;
				baseline = atoi(argument);
				if(strcmp(argument, "c") == 0){
					baseline = le_current_line + 1;
				}else if(strcmp(argument, "l") == 0){
					baseline = le_lines;
				}
				line = atoi(argument + range_index + 1);
				if(range == -1){
					baseline -= line - 1;
					line--;
				}
			}
			line--;
			for(i = 0; i < baseline + line && le_buffer[i] != NULL; i++){
				size_t size = 0;
				if(i >= baseline - 1){
					free(le_buffer[i]);
					le_buffer[i] = USR1;
				}
			}
			char** tmp_buffer = malloc(sizeof(*tmp_buffer));
			tmp_buffer[0] = NULL;
			int counter = 0;
			for(i = 0; i < le_lines; i++){
				if(le_buffer[i] != USR1 && le_buffer[i] != NULL){
					le_global_mode->edited = 1;
					tmp_buffer = realloc(tmp_buffer, sizeof(*tmp_buffer) * (counter + 2));
					tmp_buffer[counter] = malloc(strlen(le_buffer[i]) + 1);
					strcpy(tmp_buffer[counter], le_buffer[i]);
					tmp_buffer[counter][strlen(le_buffer[i])] = 0;
					free(le_buffer[i]);
					tmp_buffer[counter + 1] = NULL;
					counter++;
				}
			}
			le_lines = counter;
			free(le_buffer);
			le_buffer = tmp_buffer;
			if(le_current_line >= counter - 1){
				le_current_line = counter - 1;
			}
#ifndef WINDOWS
		}else if(strcmp(command, "rs") == 0 || strcmp(command, "regexsearch") == 0){
			if(strlen(argument) == 0){
				status->status = LE_ERROR;
				fprintf(stderr, "%s", le_get_message("ARGNEEDED"));
			}else{
				regex_t regex;
				int result = regcomp(&regex, argument, REG_EXTENDED);
				char matched = 0;
				if(!result){
					int i;
					for(i = le_current_line; le_buffer[i] != NULL; i++){
						regmatch_t* match = malloc(sizeof(*match));
						if(!(result = regexec(&regex, le_buffer[i], 1, match, 0))){
							char* before_match = malloc(match[0].rm_so + 1);
							before_match[match[0].rm_so] = 0;
							memcpy(before_match, le_buffer[i], match[0].rm_so);
							char* after_match = malloc(strlen(le_buffer[i]) - match[0].rm_eo + 1);
							after_match[strlen(le_buffer[i]) - match[0].rm_eo] = 0;
							memcpy(after_match, le_buffer[i] + match[0].rm_eo, strlen(le_buffer[i]) - match[0].rm_eo);
							char* matched_string = malloc(match[0].rm_eo - match[0].rm_so + 1);
							matched_string[match[0].rm_eo - match[0].rm_so] = 0;
							memcpy(matched_string, le_buffer[i] + match[0].rm_so, match[0].rm_eo - match[0].rm_so);
							printf("%6d: %s\x1b[7m%s\x1b[0m%s\n", i + 1, before_match, matched_string, after_match);
							free(before_match);
							free(match);
							matched = 1;
							break;
						}
						free(match);
					}
					regfree(&regex);
					if(!matched){
						printf("%s", le_get_message("REGEX_NOMATCHES"));
					}
				}else{
					status->status = LE_ERROR;
					fprintf(stderr, "%s", le_get_message("REGEX_SYNERR"));
				}
			}
		}else if(strcmp(command, "rss") == 0 || strcmp(command, "regexsearchseek") == 0){
			if(strlen(argument) == 0){
				status->status = LE_ERROR;
				fprintf(stderr, "%s", le_get_message("ARGNEEDED"));
			}else{
				regex_t regex;
				int result = regcomp(&regex, argument, REG_EXTENDED);
				char matched = 0;
				int matched_line = 0;
				if(!result){
					int i;
					for(i = le_current_line; le_buffer[i] != NULL; i++){
						regmatch_t* match = malloc(sizeof(*match));
						if(!(result = regexec(&regex, le_buffer[i], 1, match, 0))){
							char* before_match = malloc(match[0].rm_so + 1);
							before_match[match[0].rm_so] = 0;
							memcpy(before_match, le_buffer[i], match[0].rm_so);
							char* after_match = malloc(strlen(le_buffer[i]) - match[0].rm_eo + 1);
							after_match[strlen(le_buffer[i]) - match[0].rm_eo] = 0;
							memcpy(after_match, le_buffer[i] + match[0].rm_eo, strlen(le_buffer[i]) - match[0].rm_eo);
							char* matched_string = malloc(match[0].rm_eo - match[0].rm_so + 1);
							matched_string[match[0].rm_eo - match[0].rm_so] = 0;
							memcpy(matched_string, le_buffer[i] + match[0].rm_so, match[0].rm_eo - match[0].rm_so);
							printf("%6d: %s\x1b[7m%s\x1b[0m%s\n", i + 1, before_match, matched_string, after_match);
							free(before_match);
							free(match);
							matched = 1;
							matched_line = i;
							break;
						}
						free(match);
					}
					regfree(&regex);
					if(!matched){
						printf("%s", le_get_message("REGEX_NOMATCHES"));
					}else{
						le_current_line = matched_line;
					}
				}else{
					status->status = LE_ERROR;
					fprintf(stderr, "%s", le_get_message("REGEX_SYNERR"));
				}
			}
#endif
		}else if(strcmp(command, "putafter") == 0 || strcmp(command, "pa") == 0){
			if(strlen(argument) == 0){
				status->status = LE_ERROR;
				fprintf(stderr, "%s", le_get_message("ARGNEEDED"));
			}else{
				le_current_line = atoi(argument);
				if(le_current_line + 1 > le_lines){
					le_buffer = realloc(le_buffer, sizeof(*le_buffer) * (2 + le_lines + le_current_line - le_lines));
					if(le_current_line - le_lines + 1 > 0){
						int i;
						for(i = le_lines; i < le_current_line + 1; i++){
							le_buffer[i] = malloc(1);
							le_buffer[i][0] = 0;
							le_buffer[i + 1] = NULL;
						}
						le_lines = le_current_line;
					}
				}
			}
		}else if(strlen(data) == 0){
		}else if(data[0] == '#'){
		}else{
			status->status = LE_ERROR;
		}
	}

	free(command);
	free(argument);

	return status;
}

int le_open(char* path, int* lines){
	le_current_line = 0;
	le_buffer = malloc(sizeof(*le_buffer));
	le_buffer[0] = NULL;
	le_lines = 0;
	if(path == NULL){
		*lines = 0;
		le_path = NULL;
		le_file = NULL;
		return LE_NEW;
	}
	le_path = malloc(strlen(path) + 1);
	le_path[strlen(path)] = 0;
	strcpy(le_path, path);
	FILE* f = fopen(path, "r");
	if(f == NULL){
		*lines = 0;
		le_file = NULL;
		return LE_NEW;
	}else{
		*lines = 0;
		while(1){
			char* buf = NULL;
			size_t size = 0;
			ssize_t length;
			*lines = *lines + 1;
			if((length = getline(&buf, &size, f)) <= 0){
				free(buf);
				break;
			}
			le_buffer = realloc(le_buffer, sizeof(*le_buffer) * (1 + (*lines)));
			le_buffer[(*lines) - 1] = malloc(length);
			le_buffer[(*lines) - 1][length - 1] = 0;
			memcpy(le_buffer[(*lines) - 1], buf, length - 1);
			le_buffer[*lines] = NULL;
			le_lines++;
			free(buf);
		}
		*lines = *lines - 1;
		le_current_line = *lines;
		fclose(f);
		le_file = fopen(path, "a+");
		fseek(le_file, 0, SEEK_SET);
		return LE_NOINFO;
	}
}

void le_close(){
	if(le_file != NULL){
		fclose(le_file);
	}
}

void le_save(){
	if(le_file == NULL){
		if(le_path == NULL) return;
		fclose(fopen(le_path, "w"));
		le_file = fopen(le_path, "a+");
	}
	le_global_mode->edited = 0;
	fseek(le_file, 0, SEEK_SET);
	fclose(fopen(le_path, "w"));
	int i;
	for(i = 0; le_buffer[i] != NULL; i++){
		fwrite(le_buffer[i], 1, strlen(le_buffer[i]), le_file);
		fwrite("\n", 1, 1, le_file);
	}
	fflush(le_file);
}

#define LE_RETURN_MESSAGE(message) free(locale);return message;
#define LE_MSG_IF(_token, lang) if(strcmp(token, #_token) == 0){LE_RETURN_MESSAGE(LE_MSG_ ## _token ## lang);}
#define LE_LANG(_locale) LE_MSG_IF(VERINFO, _locale);\
		LE_MSG_IF(USAGE, _locale);\
		LE_MSG_IF(OPTION, _locale);\
		LE_MSG_IF(ONLY_FILENAME, _locale);\
		LE_MSG_IF(HELP, _locale);\
		LE_MSG_IF(VERSION, _locale);\
		LE_MSG_IF(COPYRIGHT, _locale);\
		LE_MSG_IF(LICENSE, _locale);\
		LE_MSG_IF(LINES, _locale);\
		LE_MSG_IF(NOFILE, _locale);\
		LE_MSG_IF(NEWFILE, _locale);\
		LE_MSG_IF(ERROR, _locale);\
		LE_MSG_IF(CHANGED, _locale);\
		LE_MSG_IF(YORNORC, _locale);\
		LE_MSG_IF(ALRDYOPEN, _locale);\
		LE_MSG_IF(OVER1, _locale);\
		LE_MSG_IF(REGEX_SYNERR, _locale);\
		LE_MSG_IF(REGEX_NOMATCHES, _locale);\
		LE_MSG_IF(ARGNEEDED, _locale);
char* le_get_message(char* token){
#if !defined(WIN32) && !defined(__MINGW32__)
	setlocale(LC_ALL, "");
	char* locale = strdup(setlocale(LC_MESSAGES, NULL));
#else
	char* locale = strdup(setlocale(LC_ALL, NULL));

#endif
#ifndef ONLY_ENGLISH
	int i;
	for(i = 0; i < strlen(locale); i++){
		if(locale[i] == '.'){
			locale[i] = 0;
			break;
		}
	}
	if(strcmp(locale, "ja_JP") == 0 || strcmp(locale, "ja") == 0 || strcmp(locale, "ja-JP") == 0){
		LE_LANG(_JA_JP);
	}else if(strcmp(locale, "sr_RS") == 0 || strcmp(locale, "sr-Latn-RS") == 0 || strcmp(locale, "sr-Latn") == 0 || strcmp(locale, "sr") == 0 || strcmp(locale, "sr-RS") == 0){
		LE_LANG(_SR_RS);
	}else if(strcmp(locale, "en_US") == 0 || strcmp(locale, "C") == 0 || strcmp(locale, "en") == 0){
#endif
		goto english;
#ifndef ONLY_ENGLISH
	}else{
		fprintf(stderr, "Unknown language `%s`, fallbacking to English\n", locale);
		goto english;
	}
#endif
	goto end;
english:
	LE_LANG(_C);
end:
	free(locale);
	return token;
}

void le_set_quiet(char quiet){
	le_quiet = quiet;
}

#if defined(NO_STRDUP)
char* strdup(char* str){
	char* result = malloc(strlen(str) + 1);
	result[strlen(str)] = 0;
	strcpy(result, str);
	return result;
}
#endif

#if defined(NO_GETLINE)
/*
 * Thanks to Will Hartung
 */
size_t getline(char **lineptr, size_t *n, FILE *stream) {
    char *bufptr = NULL;
    char *p = bufptr;
    size_t size;
    int c;

    if (lineptr == NULL) {
        return -1;
    }
    if (stream == NULL) {
        return -1;
    }
    if (n == NULL) {
        return -1;
    }
    bufptr = *lineptr;
    size = *n;

    c = fgetc(stream);
    if (c == EOF) {
        return -1;
    }
    if (bufptr == NULL) {
        bufptr = malloc(128);
        if (bufptr == NULL) {
            return -1;
        }
        size = 128;
    }
    p = bufptr;
    while(c != EOF) {
        if ((p - bufptr) > (size - 1)) {
            size = size + 128;
            bufptr = realloc(bufptr, size);
            if (bufptr == NULL) {
                return -1;
            }
        }
        *p++ = c;
        if (c == '\n') {
            break;
        }
	c = fgetc(stream);
    }

    *p++ = '\0';
    *lineptr = bufptr;
    *n = size;

    return p - bufptr - 1;
}
#endif
