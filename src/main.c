#ifdef __linux__
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <locale.h>

#if defined(WIN32) || defined(__MINGW32__)
#include <windows.h>
#endif

#include <le.h>

#define FIRSTCHAR(msg) ((strlen(msg) > 0) ? (msg[0]) : ('-'))

#if defined(WIN32) || defined(__MINGW32__)
BOOL WINAPI ctrl_c_handler(DWORD signal){
	return TRUE;
}
#endif

void le_help(FILE* out, char** argv){
	fprintf(out, le_get_message("VERINFO"), LE_TARGET, LE_VERSION, LE_DATE, __DATE__, __TIME__);
	fprintf(out, "\n");
	fprintf(out, le_get_message("USAGE"), argv[0]);
	fprintf(out, "\n");
	fprintf(out, "%s", le_get_message("OPTION"));
	fprintf(out, "%s", le_get_message("ONLY_FILENAME"));
	fprintf(out, "%s", le_get_message("HELP"));
	fprintf(out, "%s", le_get_message("VERSION"));
}

int main(int argc, char** argv){
	printf("\x1b[0m");
	fflush(stdout);
	char opt;
	char quiet = 0;
	int i;
	for(i = 0; i < argc; i++){
		if(strcmp(argv[i], "--version") == 0 || strcmp(argv[i], "-V") == 0){
				printf(le_get_message("VERINFO"), LE_VERSION, LE_TARGET, LE_DATE, __DATE__, __TIME__);
				printf(le_get_message("COPYRIGHT"), LE_COPYRIGHT);
				printf("%s", le_get_message("LICENSE"));
				return 0;
		}else if(strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0){
			le_help(stdout, argv);
			return 0;
		}else if(strcmp(argv[i], "--quiet") == 0 || strcmp(argv[i], "-q") == 0){
				quiet = 1;
		}else if(strcmp(argv[i], "--") == 0){
			break;
		}else{
			if(argv[i] == NULL) goto skip_while;
			if(strlen(argv[i]) > 0 && argv[i][0] != '-') continue;
			printf("%s: unknown option -- %s\n", argv[0], argv[i] + 1 + (strlen(argv[i]) > 1 && argv[i][1] == '-' ? 1 : 0));
			le_help(stderr, argv);
			return 1;
			break;
		}
	}
	skip_while:
#if !defined(WIN32) && !defined(__MINGW32__)
	signal(SIGKILL, SIG_IGN);
	signal(SIGINT, SIG_IGN);
#else
	signal(SIGINT, SIG_IGN);
	if(!SetConsoleCtrlHandler(ctrl_c_handler, TRUE)){
		fprintf(stderr, "failed to set the ctrl-c handler\n");
		return -1;
	}
#endif
	int lines = 0;
	char* path = argv[argc - 1] == argv[0] ? NULL : ((FIRSTCHAR(argv[argc - 1]) == '-') ? NULL : argv[argc - 1]);
	int openstat = le_open(path, &lines);
	if(!quiet){
		if(openstat == LE_NOINFO){
			printf(le_get_message("LINES"), path, lines);
		}else if(openstat == LE_NEW){
			printf("%s", path == NULL ? le_get_message("NOFILE") : path);
			if(path != NULL) printf("%s", le_get_message("NEWFILE"));
			printf("\n");
		}
	}
	le_set_quiet(quiet);
	size_t size = 0;
	ssize_t length;
	struct le_mode* mode = malloc(sizeof(*mode));
	memset(mode, 0, sizeof(*mode));
	while(1){
		if(!mode->insert && !mode->replace && !quiet) printf(":");
		fflush(stdout);
		char* buffer = NULL;
		length = getline(&buffer, &size, stdin);
		if(length == -1){
			if(feof(stdin)){
				if(!quiet) printf("eof\n");
				le_close();
				return 0;
			}
			fprintf(stderr, "stdin err or eof\n");
			return -1;
		}
		buffer[length - 1] = 0;
		struct le_status* status = le_process(buffer, mode);
		if(status->status == LE_SUCCESS){
		}else if(status->status == LE_SKIPNEXT){
		}else if(status->status == LE_ERROR){
			fprintf(stderr, "%s", le_get_message("ERROR"));
		}else if(status->status == LE_EXIT){
			if(mode->edited){
				printf("%s", le_get_message("CHANGED"));
				fflush(stdout);
				char chr;
tryagain:
				chr = getchar();
				if(chr == 'y' || chr == 'Y'){
					le_save();
				}else if(chr == 'n' || chr == 'N'){
					exit(0);
				}else if(chr == 'c' || chr == 'C'){
					fseek(stdin, 1, SEEK_CUR);
					continue;
				}else{
					fseek(stdin, 1, SEEK_CUR);
					fprintf(stderr, "%s", le_get_message("YORNORC"));
					goto tryagain;
				}
				fseek(stdin, 1, SEEK_CUR);
				le_close();
				exit(0);
			}else{
				exit(0);
			}
		}else if(status->status == LE_SAVEEXIT){
			le_save();
			le_close();
			exit(0);
		}else if(status->status == LE_NOSAVEEXIT){
			le_close();
			exit(0);
		}else{
			printf("%d\n", status->status);
		}
		free(status);
		free(buffer);
		size = 0;
		length = 0;
		buffer = NULL;
	}
	return 0;
}
